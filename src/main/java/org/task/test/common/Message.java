package org.task.test.common;

import java.io.Serializable;
import java.util.Objects;

public class Message implements Comparable, Runnable, Serializable {
    private int priority;
    private String message;

    public Message(Integer priority, String message) {
        this.priority = priority;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public int getPriority() {
        return priority;
    }

    @Override
    public int compareTo(Object o) {
        return ((Message) o).getPriority() - this.getPriority();
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
            System.out.println("Priority: " + this.getPriority() +
                    " Thread id: " + Thread.currentThread().getId() + " Message: " + this.getMessage());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean equals(Object that) {
        return Objects.equals(this, that);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this);
    }
}
