package org.task.test.consumer;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

public class ProcessingFailurePolicy implements RejectedExecutionHandler {

    /**
     * A handler for rejected tasks that throws a
     * {@code RejectedExecutionException}.
     */

    public ProcessingFailurePolicy() {
    }

    /**
     * Prints statement and throws RejectedExecutionException.
     *
     * @param r the runnable task requested to be executed
     * @param e the executor attempting to execute this task
     * @throws RejectedExecutionException always
     */
    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
        System.out.println("Handling error");

        throw new RejectedExecutionException("Task " + r.toString() +
                " rejected from " +
                e.toString());
    }
}
