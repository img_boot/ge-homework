package org.task.test.consumer;

import com.sun.net.httpserver.HttpServer;

import java.net.InetSocketAddress;
import java.util.concurrent.*;

public class Consumer {
    private static final int CORE_POOL_SIZE, MAX_POOL_SIZE;
    private static final long KEEP_ALIVE_TIME = 0;
    private static final int MAIN_THREAD = 1;
    private static final int HTTP_PORT = 8000;

    static ExecutorService executorService;

    static {
        int threadNum = Runtime.getRuntime().availableProcessors() - MAIN_THREAD;
        CORE_POOL_SIZE = MAX_POOL_SIZE = threadNum > 0 ? threadNum : 1;
        executorService = newPriorityBaseThreadPool();
    }

    public static void main(String[] args) throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(HTTP_PORT), 0);
        server.createContext("/test", new MyHandler());
        server.setExecutor(null); // creates a default executor
        server.start();
    }

    private static ExecutorService newPriorityBaseThreadPool() {
        return new ThreadPoolExecutor(CORE_POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE_TIME, TimeUnit.SECONDS,
                new PriorityBlockingQueue<>(), new ProcessingFailurePolicy());
    }
}
