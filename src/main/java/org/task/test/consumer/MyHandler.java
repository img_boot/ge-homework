package org.task.test.consumer;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.task.test.common.Message;

import java.io.IOException;
import java.io.ObjectInputStream;

class MyHandler implements HttpHandler {

    private static final int OK_CODE = 200;
    private static final int ERROR_CODE = 400;
    private static final int CONTENT_LENGTH = 0;


    public void handle(HttpExchange t) throws IOException {

        try {
            ObjectInputStream objIn = new ObjectInputStream(t.getRequestBody());
            Message message = (Message) objIn.readObject();
            objIn.close();
            Consumer.executorService.execute(message);
        } catch (Exception e) {
            t.sendResponseHeaders(ERROR_CODE, CONTENT_LENGTH);
            System.out.println("Message is broken:" + e.getMessage());
        }

        t.sendResponseHeaders(OK_CODE, CONTENT_LENGTH);
        t.close();
    }
}
