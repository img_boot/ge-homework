package org.task.test.producer;

import org.task.test.common.Message;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Random;

import static org.task.test.producer.Producer.messagePriority;
import static org.task.test.producer.Producer.testPriority;

public class GenerateMessageEvent implements Runnable {

    private static final int RANDOM_LIMIT = 20;
    private static final int MAX_PRIORITY = 10;

    @Override
    public void run() {
        for (int i = 0; i < new Random().nextInt(RANDOM_LIMIT); i++) {
            int priority = testPriority ? messagePriority : new Random().nextInt(MAX_PRIORITY);
            try {
                Producer.blockingQueue.put(new Message(priority, generateMessage()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private String generateMessage() {
        String randomString = "";
        byte[] array = new byte[7]; // length is bounded by 7
        new Random().nextBytes(array);
        try {
            randomString = URLEncoder.encode(new String(array), "UTF-8");
        } catch (UnsupportedEncodingException ignored) {
        }
        return randomString;
    }
}
