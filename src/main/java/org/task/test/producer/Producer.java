package org.task.test.producer;

import org.task.test.common.Message;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.*;

public class Producer {
    private final static int OK = 200;
    private final static int ONE_SECOND = 1000;
    private final static int CONSUMER_OFFLINE = -1;
    private final static int QUEUE_SIZE = 1000;

    //for variables for testing;
    static int messagePriority = -1;
    static boolean testPriority = false;
    static boolean simulateDeliveryProblems = false;

    static BlockingQueue<Message> blockingQueue;

    static {
        blockingQueue = new LinkedBlockingQueue<>(QUEUE_SIZE);
    }

    public static void main(String[] args) throws InterruptedException {

        Arrays.stream(args).forEach(arg -> {
            String key = arg.split("=")[0];
            String value = arg.split("=")[1];
            switch (key) {
                case "messagePriority":
                    messagePriority = Integer.valueOf(value);
                    break;
                case "testPriority":
                    testPriority = Boolean.valueOf(value);
                    break;
                case "simulateDeliveryProblems":
                    simulateDeliveryProblems = Boolean.valueOf(value);
                    break;
            }
        });

        int initialDelay = testPriority ? 0 : new Random().nextInt(5000);
        int delay = testPriority ? 500 : new Random().nextInt(5000);

        //start generating random messages
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleWithFixedDelay(new GenerateMessageEvent(), initialDelay, delay, TimeUnit.MILLISECONDS);

        //start sending messages
        startSender();
    }

    private static void startSender() throws InterruptedException {
        while (true) {
            if (!blockingQueue.isEmpty()) {
                Message m = blockingQueue.peek();
                System.out.println("Sending priority: " + m.getPriority() + " message " + m.getMessage());

                //send message
                int status = JavaPostRequest.sendPost(m);

                //naive handle success/failed delivery
                if (status == OK) {
                    blockingQueue.poll();
                } else if (status == CONSUMER_OFFLINE) {
                    Thread.sleep(ONE_SECOND);
                } else {
                    Message errorMessage = blockingQueue.poll();
                    Message correctMessage = new Message(errorMessage.getPriority(),
                            "fail over message: " + errorMessage.getMessage());
                    addFailedMessageBacktoQueue(correctMessage);
                }
            }
        }
    }

    private static void addFailedMessageBacktoQueue(Message correctMessage) {
        new Thread(() -> {
            try {
                blockingQueue.put(correctMessage);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
