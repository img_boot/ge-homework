package org.task.test.producer;

import org.task.test.common.Message;

import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import static org.task.test.producer.Producer.simulateDeliveryProblems;

public class JavaPostRequest {

    private static HttpURLConnection con;
    private static final String URL = "http://localhost:8000/test";

    public static int sendPost(Message message) {
        int status = 0;
        try {
            URL myUrl = new URL(URL);
            con = (HttpURLConnection) myUrl.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setUseCaches(false);

            // send object
            ObjectOutputStream objOut = new ObjectOutputStream(con.getOutputStream());

            //Simulate network error
            if (simulateDeliveryProblems && new Random().nextInt(10) < 3) {
                System.out.println("-- Simulate delivery error --");
                objOut.writeObject(null);
            } else {
                //send as per normal
                objOut.writeObject(message);
            }
            objOut.flush();
            objOut.close();

            status = con.getResponseCode();
        } catch (ConnectException e) {
            status = -1;
            System.out.println("No consumer found try again later");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            con.disconnect();
        }
        return status;
    }
}
